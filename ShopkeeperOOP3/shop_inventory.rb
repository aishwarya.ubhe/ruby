require_relative 'product.rb'
require_relative 'order.rb'
class UserCheck
  def initialize
    @obj_product = Product.new
    @obj_order = Order.new
  end

  def get_input()
    puts "You are a customer or shopkeeper?"
    puts "1.Shopkeeper"
    puts "2.Customer"
    user_type=gets.to_i

    if user_type == 1
      puts "1.Add Product"
      puts "2.Remove Product"
      puts "3.List Product"
      puts "4.Search Product"
      puts "5.Edit product"
      puts "Select option"
      select_option = gets.to_i
      case select_option
        when 1
          puts "1.Add Product"
          @obj_product.add_product()
        when 2
          puts "2.Remove Product"
          @obj_product.remove_product()
        when 3
          puts "3.List Product"
          @obj_product.list_product()
        when 4
          puts "4.Search Product"
          @obj_product.search_product()
        when 5
          puts "5.Edit product"
          @obj_product.edit_product()
      end
    elsif user_type >= 2 and user_type == 2
      puts "1.List Product"
      puts "2.Search Product"
      puts "3.Buy Product"
      select_option = gets.to_i
      case  select_option
        when 1
          puts "1.List Product"
          @obj_product.list_product()
        when 2
          puts "2.Search Product"
          @obj_product.search_product()
        when 3
          puts "3.Buy Product"
          @obj_order.buy_product()
      end
    else
      puts "Enter right choice"
    end
  end
end

obj_user=UserCheck.new
obj_user.get_input

