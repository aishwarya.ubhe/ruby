require_relative 'handle_files.rb'
class Product
  def initialize
    @product_id,@product_name,@price,@quantity,@company_name=0,0,0,0,0
#    @array=[]
    @obj_handle=HandleFiles.new()
  end

  def add_product()
    puts "Enter product id"
    @product_id=gets.chomp.to_i
    puts "Enter product name"
    @product_name=gets.chomp
    puts "Enter product price"
    @price=gets.chomp.to_f
    puts "Enter product quantity"
    @quantity=gets.chomp.to_i
    puts "Enter product company name"
    @company_name=gets.chomp
    @array=@product_id,@product_name,@price,@quantity,@company_name
    puts "#{@array}"
    @obj_handle.inventory(@array)
  end

  def remove_product()
    puts "Enter product_id to remove"
    @product_id=gets.chomp.to_i
    file=File.open("file_name.txt","r")
    file.delete_if{ |ele| ele == @product_id }
    file.close
  end

  def list_product()
    file=File.open("file_name.txt","r")
    file.each{ |ele| puts ele }
    file.close
  end

  def search_product()
    puts "Enter product name that you want to search "
    search_name=gets.chomp
    file=File.open("file_name.txt","r")
    search=file.select{ |ele| puts ele == search_name }
    file.close
    #puts search
  end

  def edit_product()
    puts "Enter product id that you want to edit"
    edit_id=gets.chomp
    file=File.open("file_name.txt","w")
    edit_product=file.select{ |ele| puts ele == edit_id }
    edit_id.replace(edit_product)
    file.close
  end
end
