#Part 1

array_1 = [2, 4, 6, 8, 10]
array_2 = [1, 5, 6, 8, 11, 12]

hash_1 = {a: 'a', b: 'b', c: 'c', d: 'd', e: 'e'}
hash_2 = {x: '10', y: '20', z: '30'}

multiple_times=10.times{puts "hello world"}
puts multiple_times

array=(30..40).to_a
puts "Array is #{array}"

combine_array=array_1+array_2
puts "Unique element array is: #{combine_array.uniq}"

puts "Even elements are:#{combine_array.select { |num|  num.even?  }}"

puts "Combine and delete arrays element if greater than 8:#{combine_array.delete_if{ |element| combine_array >8}}"

puts array_1
addition=array_1.inject(0){ |sum,number| sum + number*number*number}
puts "Cube of array is:#{addition}"

puts "Index of 8 is:#{combine_array.find_index(8)}"

puts "#{array_1.map{|number| puts number += 5}}"

array_3=hash_1.keys
puts "Replacing values of hash with array: #{Hash[array_3.zip(array_1)]}"

puts "Sum of integer values of hash: #{hash_2.values.inject(0){|sum,number| sum += number.to_i}}"


#Part 2

numbers_hash = {
  'user_1'  => [1,2,3,4,5,6,7,9],
  'user_2'  => [1,2,3],
  'user_3'  => [1,2,3,6,7,8],
  'user_4'  => [1,2,3,5,6,8],
  'user_5'  => [1,2,3,10]
}


converted_array=[]
converted_into_array=numbers_hash.values.each{|element| converted_array.concat(element)}
common_elements=converted_array.select{|element| converted_array.count(element)>4}.uniq
puts "Common elements are#{common_elements}"

non_common_elements= numbers_hash.values.map{|element| element-common_elements }
puts "Non common elements are#{non_common_elements}"

puts "Hash whose values more than 26 are: #{numbers_hash.select{|key,value| value.sum{|i| i}>26}}"

puts "Hash whose elements more than 4 are: #{numbers_hash.select{|key,value| value.length>4}}"

#Part 3
#Block

def block_method(first_number,second_number,&block)
  block.call(first_number,second_number)
  #yield(1,2)
end
block_method(4,2) { |n1,n2| puts n1 + n2}
block_method(4,2) { |n1,n2| puts n1 - n2}
block_method(4,2) { |n1,n2| puts n1 * n2}
block_method(4,2) { |n1,n2| puts n1 / n2}

