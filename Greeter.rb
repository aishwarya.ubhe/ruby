class Greeter
  attr_accessor :name
  def initialize(name="aish")
     @name=name
  end
  def say_hi
     puts "Hi #{@name}"
  end
  def say_bye
     puts "bye #{@name}, come soon"
   end
end

g=Greeter.new("john")
g.respond_to?("name")
g.respond_to?("name=")
g.say_hi
g.say_bye

g.name="ubhe"
g
g.say_hi
g.say_bye