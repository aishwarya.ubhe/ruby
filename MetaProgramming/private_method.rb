class Friend
  def initialize(friend)
    @friend = friend
  end

  private
  def all
    puts @friend
  end
end

new_friend = Friend.new(23)
new_friend.send(:all)