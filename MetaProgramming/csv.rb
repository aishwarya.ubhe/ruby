require 'csv'
class GenerateClass
  def initialize
    @file_name="locations.csv"
    @array=Array.new
  end

  def get_file
    file_name_regexp=/\A[a-zA-Z]\w*/
    class_name=@file_name.capitalize
    puts class_name
    puts class_name.class
    @final_cn=class_name.match(file_name_regexp)
    puts "class name is#{@final_cn}"
    @final_class_name=@final_cn.to_s
    p "class is#{@final_class_name.class}"
  end

  def read_csv
    @table=CSV.parse(File.read(@file_name),headers: false)
    @array_table=@table[0]
    @array_table.each {|attr| @array.push(attr.to_sym)}
  end

   def create_class
    new_name=@final_class_name
    new_name.slice! "s"
    var = new_name.to_sym
    puts var.class
    puts var
    newarray = @array
    @klass = Object.const_set new_name,Struct.new(*newarray)
  end

  def display_data
    puts @array.to_s
    @table = @table.drop(1)
    @table.each do |element|
      p element
    end
  end
end

gc=GenerateClass.new
gc.get_file
gc.read_csv
gc.create_class
gc.display_data