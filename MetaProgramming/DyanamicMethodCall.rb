class DyanamicMethodCall
  def initialize(foo)
    @foo=foo
  end

  def perform()
   puts "enter action to perform"
   action=gets.chomp
   p @foo.send action
  end

  def generator_method()
  end
end

dmc=DyanamicMethodCall.new(34)
dmc.perform()

