class ClassFactory
  def self.create_class(new_class, *fields)
    c = Class.new do
      fields.each do |field|
        define_method field.intern do
          instance_variable_get("@#{field}")
        end
        define_method "#{field}=".intern do |arg|
          instance_variable_set("@#{field}", arg)
        end
      end
    end

    Kernel.const_set new_class, c
  end
end
ClassFactory.create_class "Car", "make", "model", "year"
new_class = Car.new
new_class.make = "Nissan"
ClassFactory.create_class "Car", "make", "model", "year"
new_class = Car.new
new_class.make = "Nissan"


([a-zA-Z]+?)(s\b|\b)

   file_name_regexp=/\A[a-zA-Z]\w*/
    @old_name=@file_name.scan(file_name_regexp)
    var = @old_name.capitalize
    puts var
    string_class_name = @old_name.to_s
    puts string_class_name.class
    @class_name=string_class_name
    puts @class_name

#   def read_csv()
#   rx = /\A(?<name>.*)\.(?<ext>.*)\z/
#   "kittens_abc.jpg".match(rx)
#   CSV.read();
#   end

# puts "enter class name with extension"
# class_name=gets
# var="#{"class_name"}".match(rx)
# puts var
# end
# def self.create_class(new_class, *fields)
#     c = Class.new do
#       fields.each do |field|
#         define_method field.intern do
#           instance_variable_get("@#{field}")
#         end
#         define_method "#{field}=".intern do |arg|
#           instance_variable_set("@#{field}", arg)
#         end
#       end
    end
#"kittens_abc.jpg".match(rx)