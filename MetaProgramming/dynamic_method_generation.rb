class Notification
  %w{ create update delete }.each do |action|
    define_method("emit_#{action}".gsub(/e$/, 'ing')) do |something|
      "#{action} is performed on #{something.class}"
    end
  end
end

 notification = Notification.new
 puts notification.emit_creating(10)
# create is performed on User
puts notification.emit_updating(10.8997)
puts notification.emit_deleting("account")