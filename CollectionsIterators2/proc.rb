def compose(proc1,proc2)
  proc2.call(10)
  return Proc.new {proc1.call(5)}
end
proc1=Proc.new{|n| puts n*2 }
proc2=Proc.new{ |n| puts n*n }
var=compose(proc1,proc2)
var.call


def compose(l1,l2)
  lambda { |n| l2.call(l1.call(n))}
end
  l1=lambda{|n| puts n** }
  l2=lambda{|n| puts n+n }
var=compose (l1,l2)
var.call(5)