class DyanamicMethodCall
  def initialize(foo)
    @foo=foo
  end

  def perform(action)
    @foo.send action
  end
end

dmc=DyanamicMethodCall.new
dmc.perform()

