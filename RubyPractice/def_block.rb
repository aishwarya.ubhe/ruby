def explicit_block(&block)
  block.call # same as yield
end
explicit_block { puts "Explicit block called" }
