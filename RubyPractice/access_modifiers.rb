# p047classaccess.rb
/class ClassAccess
  def m1          # this method is public
    puts "public"
  end
  protected
    def m2        # this method is protected
      puts "protected"
    end
  private
    def m3        # this method is private
      puts "private"
    end
end
ca = ClassAccess.new
ca.m1
#ca.m2
#ca.m3 #: private method `m3' called for #<ClassAccess:0x0000000001718878> (NoMethodError)/

# p047zclassaccess.rb
/class Person
  def initialize(age)
    @age = age
  end
  def age
    @age
  end
  def compare_age(c)
    if c.age > age
      "The other object's age is bigger."
    else
      "The other object's age is the same or smaller."
    end
  end
  protected :age
end

chris = Person.new(25)
marcos = Person.new(34)
puts marcos.compare_age(chris)
#puts marcos.age
#puts chris.compare_age(marcos)
#puts chris.age/

/class Song
  def initialize(name, artist)
    @name     = name
    @artist   = artist
  end
  def name
    @name
  end
  def artist
    @artist
  end
end


song = Song.new("Brazil", "Ivete Sangalo")
puts song.name
puts song.artist/

/class Song
  def initialize(name, artist)
    @name     = name
    @artist   = artist
  end
  attr_reader :name, :artist  # create reader only
  # For creating reader and writer methods
  # attr_accessor :name
  # For creating writer methods
  # attr_writer :name
song = Song.new("Brazil", "Ivete Sangalo")
puts song.name
puts song.artist
end/

/class C
  def initialize
    @n = 100
  end

  def increase_n
    @n *= 20
    #puts @n
  end
end

class D < C
  def show_n
    puts "n is #{@n}"
  end
end

d = D.new
d.increase_n
d.show_n/
/
x=1
def m
  puts "hi"
end

puts self/

# p063xself1.rb
/class S
  puts 'Just started class S'
  puts self
  module M
    puts 'Nested module M or S::M'
    puts self
  end
  puts 'Back in the outer level of S'
  puts self
end/

# p063xself2.rb
/class S
  def m
    puts 'Class S method m:'
    puts self
  end
end
s = S.new
s.m/

/obj = Object.new
def obj.show
  print 'I am an object: '
  puts "here's self inside a singleton method of mine:"
  puts self
end
obj.show
print 'And inspecting obj from outside, '
puts "to be sure it's the same object:"
puts obj/

/class S
  def m
    puts "Class method of class S"
    puts self
  end
   puts self
end
obj=S.new
obj.m/


class SayHello
  def self.from_the_class
   puts  "Hello, from a class method"
  end

  def from_an_instance
    puts "Hello, from an instance method"
  end
end