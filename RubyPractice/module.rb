/module RubyM
  def add
    puts  "module add "
  end

  def sub
    puts "module sub"
  end

  def mul
    puts"module mul"
  end
end

class RubyC
  include RubyM
  def add_c
    puts "add class"
  end
end

r=RubyC.new
r.add
r.sub
r.mul
r.add_c/



module ScopeM
  c=10
  def ScopeM.add
    puts "add module"
  end

  def ScopeM.sub
    puts "sub module"
  end

  def ScopeM.mul
    puts "mul module"
  end
end

ScopeM::c

ScopeM.add
ScopeM.sub
ScopeM.mul