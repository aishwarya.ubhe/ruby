def block_method(first_number,second_number,&block)
  block.call(first_number,second_number)
  /yield(1,2)/
end
block_method(4,2) { |n1,n2| puts n1 + n2}
block_method(4,2) { |n1,n2| puts n1 - n2}
block_method(4,2) { |n1,n2| puts n1 * n2}
block_method(4,2) { |n1,n2| puts n1 / n2}

