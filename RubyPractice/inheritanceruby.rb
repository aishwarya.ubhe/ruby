/class Animal
  def make_noise
    puts "aqwww"
  end
end

class Dog < Animal
  def make_noise
    puts "barking"
  end
end

class Cat < Animal
  def make_noise
    puts "mau"
  end
end

c= Cat.new
c.make_noise/
/
class Mammal
  def breathe
    puts "inhale and exhale"
  end
end

class Cat < Mammal
  def breathe
    puts "Meow"
  end
end

rani = Cat.new
rani.breathe/


/class Bird
  def preen
    puts "I am cleaning my feathers."
  end
  def fly
    puts "I am flying."
  end
end

class Penguin < Bird
  def fly
    puts "Sorry. I'd rather swim."
  end
end

p = Penguin.new
p.preen
p.fly/

/class GF
 def initialize
  puts 'In GF class'
 end
 def gfmethod
  puts 'GF method call'
 end
end

# class F sub-class of GF
class F < GF
 def initialize
  puts 'In F class'
 end
end

# class S sub-class of F
class S < F
 def initialize
  puts 'In S class'
 end
end
son = S.new
son.gfmethod/


class Dog
  def initialize(breed="hii-")
    puts "hii 1"
    @breed = breed
  end
end

class Lab < Dog
  def initialize(breed, name)
    puts "hii 2"
    super(breed)
    @name = name
  end

  def to_s
    puts "hii 3"
    "(#@breed, #@name)"
  end
end

puts Lab.new("Labrador", "Benzy").to_s